=== Limit Excerpt ===
Contributors: alatygel
Tags: wordpress, title, excerpt, limit
Requires at least: 3.9
Tested up to: 5.4
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A very simple plugin to warn authors abouth the lenght of title and excerpt.

== Description ==

The limit excerpt plugin helps editors to give a uniformity in titles and excerpts. It is aimed at websites written by several authors, where the template is sensible to title and excerpt lengths.

The plugin offers only a warning when title or excerpt goes beyoind the limit. It doesn't block saving longer texts, nor edit saved posts.

== Installation ==

Installing "Limit Excerpt" can be done either by searching for "Limit Excerpt" via the "Plugins > Add New" screen in your WordPress dashboard, or by using the following steps:

1. Download the plugin via WordPress.org
2. Upload the ZIP file through the 'Plugins > Add New > Upload' screen in your WordPress dashboard
3. Activate the plugin through the 'Plugins' menu in WordPress

== Screenshots ==

1. Title limit applied to post editor
2. Excerpt limit applied to post editor
3. Settings

== Changelog ==

= 1.0 =
* 2020-04-27
* Initial release

== Upgrade Notice ==

= 1.0 =
* 2020-04-27
* Initial release
