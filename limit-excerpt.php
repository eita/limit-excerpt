<?php
/**
 * Plugin Name: Limit Excerpt
 * Version: 1.0.0
 * Plugin URI: https://gitlab.com/eita/limit-excerpt
 * Description: A very simple plugin to warn authors abouth the lenght of title and excerpt.
 * Author: Alan Tygel/Cooperativa EITA
 * Author URI: https://eita.coop.br
 * Requires at least: 4.0
 * Tested up to: 5.4
 *
 * Text Domain: limit-excerpt
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Alan Tygel
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load plugin class files.
require_once 'includes/class-limit-excerpt.php';
require_once 'includes/class-limit-excerpt-settings.php';

// Load plugin libraries.
require_once 'includes/lib/class-limit-excerpt-admin-api.php';
require_once 'includes/lib/class-limit-excerpt-post-type.php';
require_once 'includes/lib/class-limit-excerpt-taxonomy.php';

/**
 * Returns the main instance of Limit_Excerpt to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object Limit_Excerpt
 */
function limit_excerpt() {
	$instance = Limit_Excerpt::instance( __FILE__, '1.0.0' );

	if ( is_null( $instance->settings ) ) {
		$instance->settings = Limit_Excerpt_Settings::instance( $instance );
	}

	return $instance;
}

limit_excerpt();
