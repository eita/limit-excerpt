/**
 * Plugin Template admin js.
 *
 *  @package WordPress Plugin Template/JS
 */

// excerpt
jQuery(document).ready(function($) {
	setTimeout( function() {
			var initial_v = $('[id^="inspector-textarea-control"]').val().length;
			$('<div id="count_e">' + initial_v + '</div>').insertAfter($('[id^="inspector-textarea-control"]'));
			if (initial_v > limits['excerpt_limit']){
				$("#count_e").attr("class","red");
			} else {
				$("#count_e").attr("class","green");
			}


			$('[id^="inspector-textarea-control"]').keyup(function(){
	  		$("#count_e").text($(this).val().length);
				if ($(this).val().length > limits['excerpt_limit']){
					$("#count_e").attr("class","red");
				} else {
					$("#count_e").attr("class","green");
				}
			});
	// title
			var initial_v = $('[id^="post-title"]').val().length;
			$('<div id="count_t">' + initial_v + '</div>').insertBefore($('[id^="post-title"]'));
			if (initial_v > limits['title_limit']){
				$("#count_t").attr("class","red");
			} else {
				$("#count_t").attr("class","green");
			}

			$('[id^="post-title"]').keyup(function(){
	  		$("#count_t").text($(this).val().length);
				if ($(this).val().length > limits['title_limit']){
					$("#count_t").attr("class","red");
				} else {
					$("#count_t").attr("class","green");
				}
			});
	}, 5000);
});
